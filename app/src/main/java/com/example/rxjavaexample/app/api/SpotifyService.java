package com.example.rxjavaexample.app.api;

import com.example.rxjavaexample.app.model.Tracks;

import retrofit.http.GET;
import retrofit.http.Query;

public interface SpotifyService {
  @GET("/search/1/track.json")
  Tracks listTracks(@Query("q") String query, @Query("page") int page);
}