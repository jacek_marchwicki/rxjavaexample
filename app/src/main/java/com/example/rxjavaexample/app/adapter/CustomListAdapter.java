package com.example.rxjavaexample.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.rxjavaexample.app.R;
import com.example.rxjavaexample.app.model.Track;
import com.google.common.collect.Lists;

import java.util.List;

import javax.inject.Inject;

public class CustomListAdapter extends BaseAdapter {

    @Inject
    LayoutInflater mLayoutInflater;

    List<Track> mElements;

    @Inject
    public CustomListAdapter() {
        mElements = Lists.newArrayList();
    }

    @Override
    public int getCount() {
        return mElements.size();
    }

    @Override
    public Object getItem(int position) {
        return mElements.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.list_item, parent, false);
            assert convertView != null;

            TextView view = (TextView) convertView.findViewById(R.id.text1);
            convertView.setTag(R.id.text1, view);
        }

        TextView textView = (TextView) convertView.getTag(R.id.text1);

        Track element = mElements.get(position);
        assert textView != null;

        textView.setText(element.name);

        return convertView;
    }

    public void addElements(List<Track> elements) {
        mElements.addAll(elements);
        notifyDataSetChanged();
    }
}
