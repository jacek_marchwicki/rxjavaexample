package com.example.rxjavaexample.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rxjavaexample.app.adapter.CustomListAdapter;
import com.example.rxjavaexample.app.dagger.ActivityModule;
import com.example.rxjavaexample.app.model.Track;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.Observer;
import rx.Subscription;

public class MainActivity extends Activity implements AbsListView.OnScrollListener {

    @InjectView(android.R.id.list)
    ListView mList;
    @InjectView(android.R.id.text1)
    TextView mText;

    @Inject
    CustomListAdapter mListAdapter;

    @Inject
    ElementsProvider mElementsProvider;

    private Subscription mListSubscription;
    private Subscription mCountSubscription;
    private boolean mIsLoadingMore;

    private Observer<List<Track>> mListObserver = new Observer<List<Track>>() {

        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
            Toast.makeText(MainActivity.this, "Error downloading data", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNext(List<Track> elements) {
            mListAdapter.addElements(elements);
            mIsLoadingMore = false;
        }
    };

    private Observer<Long> mCountObserver = new Observer<Long>() {

        long mCount;

        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
            Toast.makeText(MainActivity.this, "Error counting", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNext(Long count) {
            mCount+=count;
            mText.setText(String.valueOf(mCount));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        App.get(this).getApplicationGraph().plus(new ActivityModule(this)).inject(this);

        mList.setAdapter(mListAdapter);
        mList.setOnScrollListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mListSubscription = mElementsProvider.subscribeTo(mListObserver);
        mCountSubscription = mElementsProvider.subscribeToCount(mCountObserver);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mListSubscription.unsubscribe();
        mCountSubscription.unsubscribe();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add) {
            Track track = new Track();
            track.name = "new element " + System.currentTimeMillis();
            mElementsProvider.addElement(track);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if(!mIsLoadingMore){
            if (firstVisibleItem + visibleItemCount == totalItemCount) {
                mIsLoadingMore = true;
                mElementsProvider.loadMore();
            }
        }
    }
}
