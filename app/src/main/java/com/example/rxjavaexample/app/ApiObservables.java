package com.example.rxjavaexample.app;

import com.example.rxjavaexample.app.api.SpotifyService;
import com.example.rxjavaexample.app.model.Track;
import com.example.rxjavaexample.app.model.Tracks;

import java.util.List;

import retrofit.RestAdapter;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ApiObservables {

    private static final String BASE_URL = "http://ws.spotify.com/";
    private static final String QUERY = "a";

    private int mLastIndex = 1;
    private final SpotifyService mService;

    public ApiObservables(){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();

        mService = restAdapter.create(SpotifyService.class);
    }

    public Observable<List<Track>> getObservable() {
        return Observable.create(new Observable.OnSubscribe<List<Track>>() {

            @Override
            public void call(Subscriber<? super List<Track>> subscriber) {
                Tracks tracks = mService.listTracks(QUERY, mLastIndex++);
                subscriber.onNext(tracks.tracks);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread());
    }
}
