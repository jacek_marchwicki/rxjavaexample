package com.example.rxjavaexample.app;

import com.example.rxjavaexample.app.model.Track;
import com.google.common.collect.Lists;

import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.functions.Func1;
import rx.subjects.ReplaySubject;

public class ElementsProvider {

    private ApiObservables mApiObservables;
    private ReplaySubject<List<Track>> mReplaySubject;

    public ElementsProvider() {
        mReplaySubject = ReplaySubject.create();
        mApiObservables = new ApiObservables();
    }

    public void addElement(Track element) {
        mReplaySubject.onNext(Lists.newArrayList(element));
    }

    public void loadMore() {
        mApiObservables.getObservable().subscribe(mReplaySubject);
    }

    public Subscription subscribeToCount(Observer<Long> countObserver) {
        return mReplaySubject.map(new Func1<List<Track>, Long>() {
            @Override
            public Long call(List<Track> elements) {
                return (long) elements.size();
            }
        }).subscribe(countObserver);
    }

    public Subscription subscribeTo(Observer<List<Track>> observer) {
        return mReplaySubject.subscribe(observer);
    }
}
