package com.example.rxjavaexample.app.dagger;

import android.app.Application;

import com.example.rxjavaexample.app.App;
import com.example.rxjavaexample.app.ElementsProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        injects = {
                App.class
        }, library = true

)
public final class AppModule {
    private final App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return app;
    }

    @Provides
    @Singleton
    ElementsProvider provideElementsManager() {
        return new ElementsProvider();
    }
}
