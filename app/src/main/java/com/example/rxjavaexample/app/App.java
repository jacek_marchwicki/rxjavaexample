package com.example.rxjavaexample.app;

import android.app.Application;
import android.content.Context;

import com.example.rxjavaexample.app.dagger.AppModule;

import dagger.ObjectGraph;

public class App extends Application {

    private ObjectGraph mObjectGraph;

    @Override
    public void onCreate() {
        super.onCreate();

        buildObjectGraphAndInject();
    }

    public void buildObjectGraphAndInject() {
        mObjectGraph = ObjectGraph.create(new AppModule(this));
        mObjectGraph.inject(this);
    }

    public ObjectGraph getApplicationGraph() {
        return mObjectGraph;
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }
}
